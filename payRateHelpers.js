const defaultOptions = {
  units: ['day', 'hour', 'hitch', 'mile', 'month', 'well', 'week'],
  standardTypes: [
    'Operating Rate',
    'Per Diem',
    'Mileage',
    'Standby Rate',
    'Travel Day Rate',
    'Cell Phone Reimbursement',
    'Custom/Other'
  ],
  defaultFields: [
    {name: 'Operating Rate', unit: 'day', fixed: true},
    {name: 'Per Diem', unit: 'day'},
    {name: 'Mileage', unit: 'mile'}
  ]
};

const getOptionsAsCompany = (company) => {
  const options = {...defaultOptions};

  if (company.default_pay_fields) {
    options.defaultFields = company.default_pay_fields;
    options.standardTypes = [
      ...company.default_pay_fields.map((field) => field.name),
      ...defaultOptions.standardTypes.filter((type) => {
        return company.default_pay_fields.find((field) => field.name.includes(type)) === undefined;
      })
    ];
  }

  return options;
};

const getOptionsAsUser = (user) => {
  let company = {};

  if (user.is_operator) {
    company = user.operator;
  } else if (user.is_vendor) {
    company = user.vendor;
  }

  return getOptionsAsCompany(company);
};

const getDifferences = (originalPayRate, newPayRate) => {
  if (originalPayRate && newPayRate) {
    const payRateDifferences = [];
    let hasDifferences = false;

    originalPayRate.fields.forEach((field) => {
      const newPayField = newPayRate.fields.find((f) => f.name == field.name);

      if (newPayField) {
        if (
          parseFloat(field.rate) === parseFloat(newPayField.rate) &&
          field.unit === newPayField.unit &&
          field.note === newPayField.note
        ) {
          payRateDifferences.push(field);
        } else {
          payRateDifferences.push({
            ...newPayField,
            originalField: field,
            changed: true
          });
          hasDifferences = true;
        }
      } else {
        payRateDifferences.push({
          deleted: true,
          name: field.name,
          originalField: field,
        });
        hasDifferences = true;
      }
    });

    newPayRate.fields.filter(
      (field) => !originalPayRate.fields.find((f) => f.name === field.name)
    ).forEach((field) => {
      hasDifferences = true;
      payRateDifferences.push({
        ...field,
        changed: true
      });
    });

    if (hasDifferences) {
      return payRateDifferences;
    }
  }

  return null;
};

export default {
  getDifferences,
  getOptionsAsUser,
  getOptionsAsCompany,
};
