export default class RequestAdapter {
  delete (url, params, data) {
    return this.request(url, 'DELETE', params, data);
  }

  get (url, params) {
    return this.request(url, 'GET', params);
  }

  post (url, params, data) {
    return this.request(url, 'POST', params, data);
  }

  put (url, params, data) {
    return this.request(url, 'PUT', params, data);
  }

  request (url, method, params, data) {
    throw ('request not implemented');
  }
}
