import enums from './enums.json';

export default {
  getMissingWorkRequirements (contractor) {
    let missingRequirements = [];

    for (let key in contractor.work_steps) {
      if (!contractor.work_steps[key]) {
        missingRequirements.push(enums.workStepsLabels[key]);
      }
    }

    if (!contractor.drug_screen_consent) {
      missingRequirements.push('Drug & Background Screen');
    }

    return missingRequirements;
  }
};
