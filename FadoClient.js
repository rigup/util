export default class FadoClient {
  static FIVE_SECONDS = 5000;
  static ONE_MINUTE = 60000;

  constructor (url) {
    this.url = url;
  }

  listeners = {all: new Set()};

  connect (token) {
    this.disconnect();

    if (this.isAvailable()) {
      this.token = token;
      this.ws = new WebSocket(this.url);
      this.ws.onopen = this.authenticate.bind(this);
      this.ws.onmessage = this.forwardMessage.bind(this);
      this.ws.onclose = this.reconnect.bind(this);

      if (window.RIGUP_ENVIRONMENT === 'development' || window.__DEV__) {
        this.ws.onerror = () => {
          console.warn('Error connecting to Fado in development. Not retrying.');
          this.disconnect();
        };
      }
    }
  }

  disconnect () {
    this.token = null;

    clearTimeout(this.reconnectTimeout);

    if (this.ws) {
      this.ws.onopen = null;
      this.ws.onmessage = null;
      this.ws.onclose = null;
      this.ws.close();
    }
  }

  reconnect () {
    if (!this.token) {
      return;
    }

    if (!this.reconnectDelay) {
      this.reconnectDelay = FadoClient.FIVE_SECONDS;
    }

    this.reconnectDelay = Math.min(this.reconnectDelay * 2, FadoClient.ONE_MINUTE);

    this.reconnectTimeout = setTimeout(() => this.connect(this.token), this.reconnectDelay);
  }

  authenticate () {
    if (this.token) {
      this.send(this.token);
      this.reconnectDelay = null;
    }
  }

  forwardMessage (message) {
    const {type, action, body} = JSON.parse(message.data);

    this.ensureListenerArray(type);

    [...this.listeners.all, ...this.listeners[type]].forEach((listener) => listener({type, action}, body));
  }

  ensureListenerArray = (type) => {
    if (!this.listeners[type]) {
      this.listeners[type] = new Set();
    }
  };

  isAvailable = () => !!this.url;

  onMessage = (type, listener) => {
    if (type === undefined) {
      this.listeners.all.add(listener);
      return () => this.listeners.all.delete(listener);
    }

    this.ensureListenerArray(type);

    this.listeners[type].add(listener);
    return () => this.listeners[type].delete(listener);
  };

  send (data) {
    if (this.ws.readyState === this.ws.OPEN) {
      this.ws.send(data);
    }
  }
}
