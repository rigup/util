export default {
  compileCertifications: function (job, profile) {
    let missingCertifications = [];
    let completeCertifications = [];
    let expiredCertifications = [];

    if (job.requirement_set) {
      job.requirement_set.certification_requirements.forEach((requirement) => {
        let mapping = profile.certifications.find(
          (c) => c.certification_id === requirement.certification_id
        );

        if (
          mapping &&
          mapping.certificate &&
          mapping.expiration_date &&
          new Date(mapping.expiration_date) <= new Date()
        ) {
          requirement.certification.certificate = mapping.certificate;
          requirement.certification.certificates = mapping.certificates;
          requirement.certification.expiration_date = mapping.expiration_date;
          expiredCertifications.push(requirement.certification);
        } else if (mapping && mapping.certificate) {
          requirement.certification.certificate = mapping.certificate;
          requirement.certification.certificates = mapping.certificates;
          requirement.certification.expiration_date = mapping.expiration_date;
          completeCertifications.push(requirement.certification);
        } else {
          if (mapping) {
            requirement.certification.missing_document_only = true;
          }

          missingCertifications.push(requirement.certification);
        }
      });
    }

    return {
      completeCertifications,
      missingCertifications,
      expiredCertifications
    };
  }
};
