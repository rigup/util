import FadoClient from './FadoClient';

describe('FadoClient', () => {
  const url = 'fadoUrl';
  let fadoClient;

  beforeEach(() => {
    fadoClient = new FadoClient(url);
  });

  describe('onMessage', () => {
    it('returns a deregister function', () => {
      const callback = jasmine.createSpy(callback);
      const deregister = fadoClient.onMessage(undefined, callback);

      expect(typeof deregister === 'function').toBeTruthy();
      expect(fadoClient.listeners.all.has(callback)).toBeTruthy();
    });

    it('removes listener when deregister is called', () => {
      const callback = jasmine.createSpy(callback);
      const deregister = fadoClient.onMessage(undefined, callback);
      expect(fadoClient.listeners.all.has(callback)).toBeTruthy();

      deregister();
      expect(fadoClient.listeners.all.has(callback)).toBeFalsy();
    });
  });

  describe('forwardMessage', () => {
    let allListener, fooListener, fooListener2, barListener;
    const message = {data: JSON.stringify({type: 'foo', action: 'create', body: 'body'})};
    const event = {type: 'foo', action: 'create'};

    beforeEach(() => {
      allListener = jasmine.createSpy('all');
      fooListener = jasmine.createSpy('foo');
      fooListener2 = jasmine.createSpy('foo2');
      barListener = jasmine.createSpy('bar');

      fadoClient.listeners = {
        all: new Set([allListener]),
        foo: new Set([fooListener, fooListener2]),
        bar: new Set([barListener]),
      };

      fadoClient.forwardMessage(message);
    });

    it('calls matching callbacks', () => {
      expect(allListener).toHaveBeenCalledWith(event, 'body');

      expect(fooListener).toHaveBeenCalledWith(event, 'body');

      expect(fooListener2).toHaveBeenCalledWith(event, 'body');
    });

    it('does not call non-matching callbacks', () => {
      expect(barListener).not.toHaveBeenCalled();
    });
  });
});
