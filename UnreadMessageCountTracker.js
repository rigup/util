export default class UnreadMessageCountTracker {
  constructor (fadoClient, requestAdapter, userId) {
    this.fadoClient = fadoClient;
    this.requestAdapter = requestAdapter;
    this.userId = userId;
    this.unreadCounts = {};
    this.totalUnread = 0;

    this.fetchUnreadCounts();
    this.setupNewMessageListener();
  }

  // public

  getTotalUnreadCount = () => this.totalUnread;

  getUnreadCount = (conversationId) => this.unreadCounts[conversationId] || 0;

  setCurrentConversation = (conversationId) => {
    this.currentConversationId = conversationId;

    if (conversationId) {
      this.markRead(conversationId);
    }
  };

  destroy = () => {
    if (this.removeListener) {
      this.removeListener();
    }
  };

  // protected

  fetchUnreadCounts () {
    this.requestAdapter.get('api/social/conversations/unread_counts').then((response) => {
      const counts = response.data;
      if (Array.isArray(counts)) {
        counts.forEach(({id, count}) => {
          this.setUnreadCount(id, count);
        });
      }
    }).catch(() => null);
  };

  setUnreadCount (conversationId, count) {
    const diff = count - this.getUnreadCount(conversationId);
    this.totalUnread += diff;
    this.unreadCounts[conversationId] = count;
  }

  incrementUnreadCount (conversationId) {
    this.setUnreadCount(conversationId, this.getUnreadCount(conversationId) + 1);
  }

  markRead (conversationId) {
    if (this.getUnreadCount(conversationId) === 0) {
      return;
    }

    this.setUnreadCount(conversationId, 0);
    this.updateLastReadMessageCreatedAt(conversationId);
  }

  updateLastReadMessageCreatedAt (conversationId) {
    this.requestAdapter.post(`api/social/conversations/${conversationId}/read`);
  }

  setupNewMessageListener () {
    this.removeListener = this.fadoClient.onMessage('social/message', (event, message) => {
      if (event.action !== 'create') {
        return;
      }

      if (message.user_id === this.userId) {
        return;
      }

      if (message.conversation_id === this.currentConversationId) {
        this.updateLastReadMessageCreatedAt(this.currentConversationId);
        return;
      }

      this.incrementUnreadCount(message.conversation_id);
    });
  }
}
